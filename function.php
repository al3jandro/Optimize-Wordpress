// Cambiar texto de "read more"
function mudar_read_more( $more ) {
    return '...<a href="'. get_permalink( get_the_ID() ) . '">Leia mais</a>';
}
add_filter( 'excerpt_more', 'mudar_read_more' );